"""Conftest Module"""
from test.smoketest.test_functions import TestFunctions
import pytest
from lib.mydevice.mydevice import MyDevice

def pytest_addoption(parser):
    """Pytest Addoption"""
    apk_url = "http://delber.com.mx/demo.apk"
    stf_url = "http://10.201.108.65:7100"
    dir_def = "LogCat"
    token = "401c0cd81b2648af84da5cdc5c98e5793113e396c347434f89f23cfb815bf090"
    parser.addoption("--filename", action="store", default="test.txt", help="file name")
    parser.addoption("--directoryname", action="store", default=dir_def, help="directory name")
    parser.addoption("--writestring", action="store", default="hola", help="write string")
    parser.addoption("--phoneid", action="store", default="ZY322LSZZF", help="phone id")
    parser.addoption("--apkurl", action="store", default=apk_url, help="apk url")
    parser.addoption("--packagenames", action="store", default="list", help="package names")
    parser.addoption("--packagename", action="store", default="b.a", help="package name")
    parser.addoption("--email", action="store", default="amx.framework@gmail.com", help="email")
    parser.addoption("--password", action="store", default="testing&123", help="user password")
    parser.addoption("--phonenumber", action="store", default="4499301932", help="phone number")
    parser.addoption("--stfurl", action="store", default=stf_url, help="OpenSTF instance URL")
    parser.addoption("--token", action="store", default=token, help="OpenSTF authorization token")

@pytest.fixture(autouse=True)
def session_start(request):
    """Start Session"""
    request.cls.file_name = request.config.getoption("--filename")
    request.cls.directory_name = request.config.getoption("--directoryname")
    request.cls.write_string = request.config.getoption("--writestring")
    request.cls.phone_id = request.config.getoption("--phoneid")
    request.cls.apk_url = request.config.getoption("--apkurl")
    request.cls.package_names = request.config.getoption("--packagenames")
    request.cls.package_name = request.config.getoption("--packagename")
    request.cls.phone_number = request.config.getoption("--phonenumber")
    request.cls.email = request.config.getoption("--email")
    request.cls.password = request.config.getoption("--password")
    request.cls.my_device = MyDevice(request.cls.phone_id)
    request.cls.test_functions = TestFunctions(request.cls.my_device)
    request.cls.stf_url = request.config.getoption("--stfurl")
    request.cls.token = request.config.getoption("--token")
