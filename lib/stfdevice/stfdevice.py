"""StfDevice Module"""
import subprocess
import re
import time
from urllib.parse import urljoin
import requests
from lib.mydevice.mydevice import MyDevice

class StfDevice(MyDevice):
    """StfDevice Class"""
    def __init__(self, url, token, device_id):
        """
        Initialize the class and save the connected device and its id as
        attributes of the class.
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        param str url: url for the OpenSTF instance, this includes http:// and
        ports if this the case.
        param str token: the user authentication token in the OpenSTF instance.
        This can be created in Settings > Keys > Access Tokens.
        param str device_id: the local id of the device.
        """
        self._local_device_id = device_id
        self._url = url
        self._token = token
        if not self.check_if_using():
            self.use_device()
        self._remote_device_id = self.get_connection_address()
        super(StfDevice, self).__init__(self._remote_device_id)
        self.connect_device()
        self.device_id = self._remote_device_id
        time.sleep(1)

    def check_if_using(self):
        """
        Verifies that the device is under control of the current user. Throws
        an exception if the state of the device could not be verified.
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        returns: boolean value indicating if device is being used
        """
        headers = {
            "Content-type": "application/json",
            "Authorization": "Bearer {}".format(self._token)
        }
        url = urljoin(self._url, "/api/v1/user/devices")
        res = requests.get(url, headers=headers)
        res.raise_for_status()
        json = res.json()
        if not json["success"]:
            raise ConnectionRefusedError(json["description"])
        for device in json["devices"]:
            if device["serial"] == self._local_device_id:
                return True
        return False

    def use_device(self):
        """
        Attempts to add a device under the authenticated user's control.
        This is analogous to pressing "Use" in the UI
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        """
        headers = {
            "Content-type": "application/json",
            "Authorization": "Bearer {}".format(self._token)
        }
        data = "{{\"serial\": \"{id}\"}}".format(id=self._local_device_id)
        url = urljoin(self._url, "/api/v1/user/devices")
        res = requests.post(url, data=data, headers=headers)
        res.raise_for_status()
        json = res.json()
        if not json["success"]:
            raise ConnectionRefusedError(json["description"])

    def connect_device(self):
        """
        Attempts to connect the remote device in ADB, throws ConnectionError
        if the connection fails. In case the device is already
        connected the connection will be considered successful.
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        """
        command = ["adb", "connect", self.get_connection_address()]
        subprocess.call(command)

    def disconnect_device(self):
        """
        Attempts to remove the ADB connection with the device. Throws
        ConnectionError if disconnection fails. In case the device
        was not connected, the disconnection will be considered
        successful.
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        """
        command = ["adb", "disconnect", self.get_connection_address()]
        c_one = subprocess.Popen(command, stdout=subprocess.PIPE)
        output = str(c_one.communicate()[0])
        output = re.sub(r"^b", "", output)
        output = re.sub(r"^\'|\'$", "", output)
        val_one = output.startswith("disconnected ")
        val_two = output.startswith("error: no such device ")
        if (not val_one) and (not val_two):
            raise ConnectionError(output)

    def get_connection_address(self):
        """
        Allows you to retrieve the remote debug URL (an address for 'adb
        connect') for a device the authenticated user controls
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        returns: The connection address for ADB
        """
        headers = {
            "Content-type": "application/json",
            "Authorization": "Bearer {}".format(self._token)
        }
        val_one = "/api/v1/user/devices/"
        url = "{}{}{}/remoteConnect".format(self._url, val_one, self._local_device_id)
        res = requests.post(url, headers=headers)
        res.raise_for_status()
        json = res.json()
        if not json["success"]:
            raise ConnectionRefusedError(json["description"])
        return json["remoteConnectUrl"]

    def remove_device(self):
        """
        Attempts to remove a device under the authenticated user's control.
        This is analogous to pressing "Stop using" in the UI
        Pre-condition: Running OpenSTF instance with both Auth keys and ADB
        keys added correctly
        """

        headers = {
            "Content-type": "application/json",
            "Authorization": "Bearer {}".format(self._token)
        }
        data = "{{\"serial\": \"{id}\"}}".format(id=self._local_device_id)
        url = urljoin(self._url, "/api/v1/user/devices/{}".format(self._local_device_id))
        res = requests.delete(url, data=data, headers=headers)
        res.raise_for_status()
        json = res.json()
        if not json["success"]:
            raise ConnectionRefusedError(json["description"])
