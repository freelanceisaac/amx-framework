"""MyApp Module"""
from abc import abstractmethod
import re
from lib.mydevice.mydevice import MyDevice


class MyApp(MyDevice):
    """MyApp Class"""
    _package_name = None

    def __init__(self, package_name, device_id):
        super().__init__(device_id)
        _package_name = package_name

    def is_opened(self):
        """
        Checks if the application has been opened by
        finding package name as the current focus
        parameter from the command dumpsys
        :returns a boolean indicating if the package
        name has been found
        """
        output = self.adb_controller.run_command(['dumpsys', 'window', 'windows'])
        find = re.findall("mCurrentFocus.*}$", output)
        if not find:
            return False
        current_focus = find[0]
        sanitized_package = self._package_name.replace(".", "\\.") + "\\/"
        return len(re.findall(sanitized_package, current_focus)) > 0

    @abstractmethod
    def is_home_screen_displayed(self):
        """Check if home screen was displayed"""
