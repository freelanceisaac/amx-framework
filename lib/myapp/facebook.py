"""Facebook Module"""
import time
from lib.myapp.i_myapp import MyApp


class Facebook(MyApp):
    """Facebook Class"""
    def __init__(self, device_id):
        super().__init__("com.facebook.katana", device_id)

    def is_home_screen_displayed(self):
        """
        Checks if the home page of Facebook is displayed in the device
        Pre-condition: have the Facebook app open in the home page
        returns: True if the home screen is displayed
        """
        return self(description="Camera").exists and self(description="Messaging").exists

    def login(self, email, password):
        """
        Receives the email and the password to log into the Facebook app
        Pre-condition: have the Facebook app open in the login page
        param str email: email of the facebook account
        param str password: password of the facebook account
        returns: True if the login was successful or False if it wasn't
        """
        self.press.enter()
        time.sleep(1)
        usr = self(text="Phone or Email")
        usr.set_text(email)
        self.press.enter()
        time.sleep(1)
        usr_pw = self(text="Password")
        usr_pw.set_text(password)
        self.press.enter()
        time.sleep(3)
        return self.is_home_screen_displayed()
