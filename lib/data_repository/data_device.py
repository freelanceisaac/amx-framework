"""Device Module"""
import random
import string

class DataDevice:
    """Device Class"""
    def __init__(self):
        self.identifier = self.id_generator(6)

    def generate_json(self, fields):
        """
        Generates the Json object
        param list fields: fields with key and value
        """
        json = {
            'id': self.identifier
        }
        for field in fields:
            json[field.key] = field.value
        return json

    @staticmethod
    def id_generator(size=6, chars=None):
        """
        Creates a random alphanumeric string
        param int size: number of characters of the string
        param seq chars: sequence of characters to be used when generating the string
        returns: an alphanumeric string
        """
        if not chars:
            chars = string.ascii_uppercase + string.digits
        return ''.join(random.choice(chars) for _ in range(size))
