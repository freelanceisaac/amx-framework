"""DataRepository Module"""
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore


class DataRepository:
    """DataRepository Class"""
    def __init__(self):
        """
        Initialize the class and creates the database object
        Pre-condition: the project has to exists in Firebase
        """
        credential = {
            "type": "service_account",
            "project_id": "amx-up-framework",
            "private_key_id": "2bdd3e8d9f354ab02a6c78591486bc92c523ef97",
            # pylint: disable=line-too-long
            "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCnVZuH6A6OSV3A\nSyHl0DbO9aC9YDjg1++hNJ3UyeGL0yoxM53jrLGZTXNWIdFFhWdqu3iPUOfe8/nr\nh1KT7oaNgGzizKDXazvHMFpDUmB4I0Bmkzrpr8u0/Onx1Ij1VW9ZPj2BGJxITWGb\nqp1MxBRw38e1xj+U164Cbadbjf5xspeqiCZn0SCQ3+8lTp5tywzwiUcHlQQBHmCm\nc1keKDe0XIDY52LP/clqkXiAM060BcEI0bhOdZlN8Hj5WRrLfObj5XuXbd1Puawf\nFotU6Vh9OgO4crp+eLZwr3CNS0az3FMQ6zTCAolYSgjujdD7Ic0muuYhlDTqrxJC\nXQhg+qh/AgMBAAECggEAMfKBrD1+KsaHb2B9y8UstvXmduCD2claw7ma6usfocb0\nLx+DH3RqxqGxraIGf0PkrJ0szUqPC6Ozk7Qe1/04uH//AGuoDuT5QOmT58hs9y8+\nh9+mlB5fJbTgd8jC4MgRpofHgANCkupWGksBDOryVwbU69MzI1CFpj/o1pE5w+HY\nlNaXcVIN1KqOIMzvJlbAihVbh+Wt3/upZnPqXBV04IURZy6geqyWWZl+3/LiC2eR\njZecSvbuNsKD/BOBfRcxcvT9KdeAyWropD6DSITem4fQIUu/LDukBoEAJzqYs+37\nY0GUEvNPA8PKaq1yIPUppfxyZ+K0ou1Y9XM/53TgoQKBgQDqTFBAkz+by/oEBQy5\nGazjvpI9KGhGy6UeMh5OZpPdxtncFRIy2RNQhyCjU92u9Ov+APFxJbbBVeCOjVYC\ndzDzmtuI6XfEaqDjkhdJ2AP8NuFBF9qH+Rk+CxTP1ISm1c7sx3P5c2NbRP5ZoVyh\n6YjcxZzC4FFs6gSnG9vs1JPLmQKBgQC21XK0jCCrwfzjXGppng7xW1ieBZTH0D3o\nJ8jY/PDcZsV9r2QfpD872aC5M4kHO0V1YslaDHZhfKwU10bI3BMEPlq+N7O9bwiU\n1/fa4B3T1CXHgde2q3G8XeVKlTJar0xNfBkl1pLM7r9QyLSJXGjgh5CdN7gikRA7\n2FKAaSLj1wKBgCVUp2zCiV3/cAR38YEEkEFxOce4ynSNOe/HuIBxY5nwdwHT8aRc\nZfFstkyPrkCUEY3VBKMsiSPg1PihmFSoPQpzYAs+d7C59IyN2FwRqQDklZtTe6Qy\nkk0KYvSCPeh/JIshdF8IfYorQgNx7Fp35TCX49RA/XhHCRq/G5lufWXxAoGBAJJx\noz/EIYMRjUpbQoy4YkWmIfPoioQFYyCphTFwYEhZNQR30J2bhF6e8SKGidI2AmuE\npbyv0L5DCt5ilDrcM4spmqtTmG86GUiHN+qjqqB6A4yqjqqs5hu3pam4mS2TzW0e\n4lGxbcIh1TGSoTCIySRW0VlsQNvHD5avrV1XWO/fAoGAbgiBgPf9w7iDF9Ow2gIE\nx4kUatU/Pcggt6E/g4Kjc9ZAXSw36SfjBsMaJ45fs+hVWW8kPn85LR3tHCe3CNCz\nsIYpLY+AkNlBWC7cWd+gakiSiJLl+TCNs/1BPT/8VxHH277TlVYtj4ieuyAa1rzG\n83D4FfgrYEpQJZrxez6Zt98=\n-----END PRIVATE KEY-----\n",
            "client_email": "firebase-adminsdk-9ad3d@amx-up-framework.iam.gserviceaccount.com",
            "client_id": "100442468796876168727",
            "auth_uri": "https://accounts.google.com/o/oauth2/auth",
            "token_uri": "https://oauth2.googleapis.com/token",
            "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
            # pylint: disable=line-too-long
            "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-9ad3d%40amx-up-framework.iam.gserviceaccount.com"
        }
        cred = credentials.Certificate(credential)
        firebase_admin.initialize_app(cred, {'projectId': 'amx-up-framework'})
        self._db = firestore.client()

    def insert_record(self, _collection, _document, _data):
        """
        Inserts a document in the specified collection
        param str _collection: name of the collection
        param str _document: id of the document
        param json _data: data of the document
        """
        self._db.collection(_collection).document(_document).set(_data)

    def update_record(self, _collection, _document, _data):
        """
        Updates a document in the specified collection
        param str _collection: name of the firebase collection
        param str _document: id of the document
        param json _data: data of the document
        """
        ref = self._db.collection(_collection).document(_document)
        ref.update(_data)

    def get_records(self, _collection):
        """
        Get all the documents from the specified collection
        param str _collection: name of the collection
        returns: an array containing the list of documents
        """
        docs = self._db.collection(_collection).get()
        return docs

    def print_records(self, _collection):
        """
        Prints all the documents from the specified collection
        param str _collection: name of the collection
        """
        docs = self._db.collection(_collection).get()
        for doc in docs:
            print(u'{} => {}'.format(doc.id, doc.to_dict()))
