# pylint: disable=R0904
"""MyDevice Module"""
import datetime
import time
import os
from urllib import request
import validators
from uiautomator import Device
from lib.adb.adbcontroller import AdbController
from lib.filemanager.filemanager import FileManager
from lib.logs.logger import Logger


class MyDevice(Device):
    """MyDevice Class"""
    def __init__(self, device_id):
        """
        Initialize the class and save the connected device and its id as attributes of the class
        Pre-condition:
        param str appName: id of the connected device
        returns:
        """
        super(MyDevice, self).__init__(device_id)
        self.device_id = device_id
        self._log_path = "LogCat"
        self.adb_controller = AdbController(self.device_id)
        self._apk_path = "Downloads"
        self._screenshot_path = "Screenshots"
        FileManager.directory_exists_create(self._log_path)
        FileManager.directory_exists_create(self._apk_path)
        FileManager.directory_exists_create(self._screenshot_path)

    def install_app(self, app_package):
        """
        The package name is received and the app is installed from PlayStore
        Pre-condition: you must know the name of the package
        param str appName: package name to install
        returns:
        """
        self.adb_controller.install_app(app_package)
        self(text="INSTALL").click()
        self(text="OPEN").wait.exists(timeout=120000)

    def download_apk(self, url):
        """
        Downloads the apk from the specified url and saves it in the pc
        Pre-condition: the url has to exists
        param str url: the url of the apk
        returns: the file path of the downloaded apk
        """
        validation = validators.url(url)
        if validation:
            FileManager.directory_exists_create(self._apk_path)
            date_time = str(datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
            file_name = os.path.join("{}/{}{}".format(self._apk_path, date_time, ".apk"))
            request.urlretrieve(url, filename=file_name)
        else:
            raise ValueError("URL not valid")
        return file_name

    def upload_install_app(self, apk_name):
        """
        Uploads from the apk_path an apk to be installed on the device
        :param apk_name: Name of the APK file
        :return: False if no APK was found in the Directory
        """
        FileManager.directory_exists_create(self._apk_path)
        file_path = os.path.join(self._apk_path, apk_name)
        exists = os.path.isfile(file_path)
        if not exists:
            return False
        self.adb_controller.upload_install_app(file_path)
        return True

    def open_app(self, app_package):
        """
        It receives the name of the app and opens it
        Pre-condition: app must be at home screen.
        param str appName: name of the app you want to launch
        returns:
        """
        return self.adb_controller.open_app(app_package)

    def close_app(self, app_package):
        """
        Method description
        Close an app
        :param str app_package: the package name to close
        """
        self.adb_controller.close_app(app_package)

    def download_log(self, file_name):
        """
        Needs to be Windows OS, Downloads the Device Logcat to the Desktop with the given name
        returns: none
        """
        FileManager.directory_exists_create(self._log_path)
        file_name = file_name.split(".")[0]
        file_path = os.path.join(self._log_path, "{}.log".format(file_name))
        file = open(file_path, "w")
        self.adb_controller.download_logcat(file)

    def open_log(self, file_name):
        """
        Receives the date of the log and opens the file
        Pre-condition: the logcat has to exist in the pc
        param str date: date of the log
        returns: file
        """
        file_name = file_name.split(".")[0]
        route = self._log_path + file_name + ".log"
        file = open(route, "r", encoding="utf8")
        return file

    @staticmethod
    def close_log(file):
        """
        Receives the string with the command that will
        be run by the adb shell. The command must be
        introduced as if the device is already in the
        adb shell.
        returns: shell output as string
        Receives the file and closes the file
        Pre-condition: the file has to exist in the pc
        param str file: file
        returns:
        """
        file.close()

    def back_button(self):
        """
        Presses the back button on the device
        :return: none
        """
        self.press.back()

    def make_phone_call_ui(self, number):
        """
        It receives a telephone number and makes the call with uiautomator
        Pre-condition: valid number and the phone app has to be on the home page
        param str number: telephone number
        """
        phone_number = ''.join(i for i in number if i.isdigit())
        if len(phone_number) == 10:
            self.screen.on()
            self.press.home()
            self(text="Phone").click()
            self(resourceId="com.android.dialer:id/floating_action_button").click()
            arr_num = list(phone_number)
            for digit in arr_num:
                self(resourceId="com.android.dialer:id/dialpad_key_number", text=digit).click()
            self(resourceId="com.android.dialer:id/dialpad_floating_action_button").click()
        else:
            raise ValueError("Incorrect Format number")
        return True

    def clear_apps(self):
        """
        Clear the open apps from device
        Pre-condition: The device must be unlocked
        cases for motorola samsung huawei and lg
        returns: True if it were able to close the all apps
        """
        #special case for motorola, scroll to show button
        self.press.recent()
        self.drag(219, 200, 219, 597)
        if self(text="BORRAR TODO").exists:
            self(text="BORRAR TODO").click()
        #case LG
        elif self(text="Clear all").exists:
            self(text="Clear all").click()
        #case Samsung
        elif self(text="CLOSE ALL").exists:
            self(text="CLOSE ALL").click()
        #case Huawei
        elif self(resourceId="com.android.systemui:id/clear_all_recents_image_button").exists:
            self(resourceId="com.android.systemui:id/clear_all_recents_image_button").click()
        else:
            return False
        return True

    def go_home(self):
        """
        Presses the home button on the device
        returns: none
        """
        self.press.home()

    def is_app_installed(self, package_name):
        """
        Receives the string with the app route that is desired
        to look for in device
        returns: true if the app is installed. false if the app is not installed
        """
        result = self.adb_controller.get_packages_list()
        return "package:{}".format(package_name) in result

    def toggle_bluetooth(self, state):
        """
        Turns on or off the bluetooth of the device
        :param bool state: Receives False to turn off and True to turn on
        :return: the output of the adb command
        """
        res = self.adb_controller.toggle_bluetooth(state)
        #Click in the alert to grant permission to turn on or tun off the bluetooth
        self(resourceId="android:id/button1").click()
        return res

    def turn_wifi(self, should_turn_on):
        """
        Receives a bool to turn on off the wifi
        param str should_turn_on: a boolean to decide the wifi status
        returns: true if wifi is turn off/on
        """
        self.adb_controller.run_command(
            ["shell",
             "am",
             "start",
             "-a",
             "android.intent.action.MAIN",
             "-n",
             "com.android.settings/.wifi.WifiSettings"])
        #Spanish
        wifi_off_spanish = self(text="Desactivado").exists
        wifi_on_spanish = self(text="Activado").exists
        #English
        wifi_off_english = self(text="OFF").exists
        wifi_on_englisnh = self(text="ON").exists
        if should_turn_on and (wifi_off_spanish or wifi_off_english):
            #19 represents the KEYCODE_DPAD_UP event
            switch_command_a = "shell input keyevent 19"
            #23 represents the KEYCODE_DPAD_CENTER event
            switch_command_b = "shell input keyevent 23"
            self.adb_controller.run_command(switch_command_a.split(' '))
            self.adb_controller.run_command(switch_command_a.split(' '))
            self.adb_controller.run_command(switch_command_b.split(' '))
        elif not should_turn_on and (wifi_on_spanish or wifi_on_englisnh):
            #19 represents the KEYCODE_DPAD_UP event
            switch_command_a = "shell input keyevent 19"
            #23 represents the KEYCODE_DPAD_CENTER event
            switch_command_b = "shell input keyevent 23"
            self.adb_controller.run_command(switch_command_a.split(' '))
            self.adb_controller.run_command(switch_command_a.split(' '))
            self.adb_controller.run_command(switch_command_b.split(' '))

    def end_call(self):
        """
        Ends an active call
        Pre-condition: There must be an active call
        returns: A boolean true if the call was ended
        """
        cmd = self.adb_controller.run_command(["shell", "input", "keyevent", "KEYCODE_ENDCALL"])
        return cmd == "b''"

    def sleep_call(self, milliseconds):
        """
        Wait the specified time
        param int milliseconds: the time to wait in milliseconds
        """
        if isinstance(milliseconds, int):
            time.sleep(milliseconds / 1000)
            self.wait(milliseconds)
        else:
            raise ValueError("You should send time as an int in milliseconds")

    def assert_call(self):
        """
        Assert if a call is answered within a minute.
        returns: A boolean true if the call is answered.
        """
        timeout = 60   # [seconds]
        timeout_start = time.time()
        while time.time() < timeout_start + timeout:
            if self(textStartsWith="00:0").exists:
                return True
        return False

    def make_click(self, elem_type, name):
        """
        Validates if the element exists and makes the click if it exists
        param str elem_type: type of the element to be clicked (class,id,text,description)
        param str name: name of the element to be clicked
        returns: True if the click is made
        """
        if elem_type == "class":
            if self(className=name).exists:
                self(className=name).click()
            else:
                raise Exception("The element does not exists")
        elif elem_type == "id":
            if self(resourceId=name).exists:
                self(resourceId=name).click()
            else:
                raise Exception("The element does not exists")
        elif elem_type == "text":
            if self(text=name).exists:
                self(text=name).click()
            else:
                raise Exception("The element does not exists")
        elif elem_type == "description":
            if self(description=name).exists:
                self(description=name).click()
            else:
                raise Exception("The element does not exists")
        return True

    def take_screenshot(self, filename):
        """
        Programmatically takes a screenshot from the device and stores it in
        the path set up for screenshots with the name given. The image file
        must include a PNG extension. The device's screen must be turned on.
        param str filename: The name for the resulting screenshot.
        """
        file_path = os.path.join(self._screenshot_path, filename)
        file = open(file_path, "w")
        self.adb_controller.take_screenshot(file)

    def pull_files(self, directories, destination_directory):
        """
        Downloads an array of files or folders to a selected destination
        param array str android_directory: an array with all the files to download
        param str destination_directory: Directory where the files will be downloaded
        """
        if isinstance(directories, list):
            for directory in directories:
                if self.pull_file(directory, destination_directory):
                    Logger.success("File " + directory + " succesfuly downloaded")
                else:
                    Logger.error("Error downloading " + directory)
        else:
            raise ValueError("Directories must be a list of strings with the files to download")

    def pull_file(self, android_directory, destination_directory):
        """
        Downloads a file or folder to a selected destination
        param str android_directory: Directory where is located the file or folder to download
        param str destination_directory: Directory where the files will be downloaded
        returns: A boolean true if it was a succesfull download
        """
        if not isinstance(android_directory, str) or not isinstance(destination_directory, str):
            raise ValueError("The android_directory and the destination_directory must be strings")
        FileManager.directory_exists_create(destination_directory)
        cmd = self.adb_controller.run_command(
            ["pull",
             android_directory,
             destination_directory])

        if not cmd:
            return True
        return False

    def find_in_log(self, file_name, value):
        """
        Finds a value in a log file
        param str file_name: Name of the log file where the value will be found
        param str destination_directory: The value to be searched in the log file
        returns: A boolean true if the value was found, false otherwise
        """
        return FileManager.value_exists(self._log_path, file_name, value)
