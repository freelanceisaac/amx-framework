"""Logger Module"""
import os
import datetime
from colorama import init, Fore
from lib.filemanager.filemanager import FileManager


class Logger:
    """Logger Class"""
    log_file = None
    log_path = "Logger"
    file_time = None

    @staticmethod
    def debug(message):
        """
        Logs a debug message
        :param message: printed message
        :return: None
        """
        Logger.log("DEBUG", Fore.RESET, message, "  -")

    @staticmethod
    def info(message):
        """
        Logs an Information message
        :param message: printed message
        :return: None
        """
        Logger.log("INFO", Fore.LIGHTBLUE_EX, message, "   -")

    @staticmethod
    def success(message):
        """
        Logs a Success message
        :param message: printed message
        :return: None
        """
        Logger.log("SUCCESS", Fore.LIGHTGREEN_EX, message, "-")

    @staticmethod
    def warning(message):
        """
        Logs a Warning message
        :param message: printed message
        :return: None
        """
        Logger.log("WARNING", Fore.LIGHTYELLOW_EX, message, "-")

    @staticmethod
    def error(message):
        """
        Logs an Error message
        :param message: printed message
        :return: None
        """
        Logger.log("ERROR", Fore.LIGHTRED_EX, message, "  -")

    @staticmethod
    def logger_content():
        """
        Gets the current logger content
        :return: The complete current logger content
        """
        log_file = Logger.check_instance()
        content = log_file.read()
        print(content)
        return content

    @staticmethod
    def check_instance():
        """
        Checks if the file instance has been instantiated
        Creates the file object if it hasn't been instantiated
        :return: The File Manager instance
        """
        if Logger.log_file is None:
            Logger.initialize_colorama()
            Logger.file_time = str(datetime.datetime.now().strftime("%Y-%m-%d %H-%M-%S"))
            FileManager.directory_exists_create(Logger.log_path)

            file_path = os.path.join("{}/{}{}".format(Logger.log_path, Logger.file_time, ".log"))
            Logger.log_file = FileManager(file_path, "w+")
        return Logger.log_file

    @staticmethod
    def log(log_type, log_color, message, separator):
        """
        Prints in a log in the console and in the log file
        :param log_type: string with the log type
        :param log_color: Fore color for the text
        :param message: Message to be printed
        :param separator: Separator between the log type and the message
        :return: None
        """
        log_file = Logger.check_instance()
        now = str(datetime.datetime.now().strftime("%H:%M:%S"))

        log_entry = now + " - " + log_type + " " + separator + " " + message
        log_file.write(log_entry + "\n")
        print(log_color + log_entry + Fore.RESET)

    @staticmethod
    def initialize_colorama():
        """
        Initializes the colorama library
        If using pycharm it is initialized accordingly
        :return: None
        """
        if 'PYCHARM_HOSTED' in os.environ:
            convert = False
            strip = False
        else:
            convert = None
            strip = None

        init(convert=convert, strip=strip)
