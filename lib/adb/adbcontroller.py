"""AdbController Module"""
import subprocess


class AdbController:
    """AdbController Class"""
    def __init__(self, device_id):
        self._command_start = ["adb", "-s", device_id]
        self.device_id = device_id

    def run_command(self, command, output_destination=subprocess.PIPE):
        """
        This method runs and adb command for a given phone
        param list command: list of strings that form the adb command
        param output_destination: where will the output of the run command be written
        returns: The output obtained after running the adb command
        """
        if not command:
            raise Exception("Command array cannot be empty")
        if not isinstance(command, list):
            raise Exception("Command must be a list of string")
        command = self._command_start + command
        c_one = subprocess.Popen(command, stdout=output_destination)
        output = str(c_one.communicate()[0])
        return output

    def install_app(self, app_package):
        """
        Launches the PlayStore app to install the application
        corresponding to the given package name
        Pre-condition: The package name must correspond to an existing
        application in the Play Store
        param str app_package: package name to install
        returns: True if the command excecutes sucesfully, False otherwise
        """
        command = ["shell", "am", "start", "-a", "android.intent.action.VIEW", "-d",
                   "'market://details?id={}'".format(app_package)]
        result = self.run_command(command, True)
        c_one = "b'Starting: Intent {{ act=android.intent.action.VIEW dat=market://details?id="
        expected = "{}{} }}".format(c_one, app_package)
        return result.startswith(expected)

    def open_app(self, app_package):
        """
        Launches the the application corresponding to the given package
        name
        Pre-condition: The package name must correspond to an installed
        application in the phone
        param str app_package: package name of the app to be launched
        returns: True if the command excecutes sucesfully, False otherwise
        """
        launcher = "android.intent.category.LAUNCHER"
        command = ["shell", "monkey", "-p", app_package, "-c", launcher, "1"]
        result = self.run_command(command)
        return "No activities found to run" not in result

    def close_app(self, app_package):
        """
        Closes the app with the given package name
        param str app_package: the package name of the app to close
        """
        command = ["shell", "am", "force-stop", app_package]
        #This command has not output, so another way to be sure that
        #it executed correctly is needed
        self.run_command(command)

    def make_phone_call_adb(self, number):
        """
        It receives a telephone number and makes the call with adb
        Pre-condition: the number has to be valid
        param str numner: telephone number
        returns: True if the command excecutes sucesfully, False otherwise
        """
        phone_number = ''.join(i for i in number if i.isdigit())
        intent_call = "android.intent.action.CALL"
        if len(phone_number) == 10:
            number = "tel:{}".format(phone_number)
            command = ["shell", "am", "start", "-a", intent_call, "-d", number]
            result = self.run_command(command)
            expected = "b'Starting: Intent { act=android.intent.action.CALL dat=tel:xxxxxxxxxx"
            result = result.startswith(expected)
        else:
            raise ValueError("Incorrect Format number")
        return result

    def download_logcat(self, file):
        """
        Saves the logcat from the phone to the PC
        param file: File in which the logcat will be saved
        returns: True if the command excecutes sucesfully, False otherwise
        """
        command = ["shell", "logcat", "*:V", "-d"]
        expected_result = "--------- beginning of crash"
        result = self.run_command(command, file)
        return result.startswith(expected_result)

    def upload_install_app(self, file_path):
        """
        Installs in the device an apk stored saved in the PC
        param str file_path: Name with path in the PC of the apk to be installed
        returns: True if the command excecutes sucesfully, False otherwise
        """
        command = ["install", file_path]
        result = self.run_command(command)
        expected_result = "b'Success'"
        return result.startswith(expected_result)

    def get_packages_list(self):
        """
        Returns the list of packages that are installed on the device
        returns: an array with the list of packages
        """
        command = ["pm list packages"]
        return self.run_command(command)

    def toggle_bluetooth(self, state):
        """
        Turns on or off the bluetooth of the device
        :param bool state: Receives False to turn off and True to turn on
        :return: the output of the adb command
        """
        if not isinstance(state, bool):
            raise TypeError("Invalid state type. Must be boolean.")
        enable = "android.bluetooth.adapter.action.REQUEST_ENABLE"
        disable = "android.bluetooth.adapter.action.REQUEST_DISABLE"
        command_on = ["shell", "am", "start", "-a", enable]
        command_off = ["shell", "am", "start", "-a", disable]
        res = ""
        if state:
            res = self.run_command(command_on)
        elif not state:
            res = self.run_command(command_off)
        return res

    def take_screenshot(self, file):
        """
        Programmatically takes a screenshot from the device and stores the
        result as a PNG. The device's screen must be turned on.
        param file: File in which the screenshot will be stored.
        """
        command = ["exec-out", "screencap", "-p"]
        self.run_command(command, file)
