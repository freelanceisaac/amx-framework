"""FileManager Module"""
import os


class FileManager():
    """FileManager Class"""
    def __init__(self, file_name, permissions):
        """
        Opens the file with the specified permissions
        Pre-condition: the file has to exist in the pc in the specified path
        param str file_name: the path of the file and the name of the file
        param str permissions: permissions to manage the file
        returns:
        """
        self.file_name = file_name
        self.permissions = permissions
        self.file_obj = open(file_name, permissions)

    def __enter__(self):
        """
        Return the file
        Pre-condition: the file has to exist in the pc
        returns: file
        """
        return self.file_obj

    def __exit__(self, exception_type, exception_value, traceback):
        """
        Closes the file
        Pre-condition: the file has to exist in the pc
        returns:
        """
        self.file_obj.close()

    def read(self):
        """
        Reads and returns the contents in the file
        Pre-condition: the file has to exist in the pc
        returns: the contents in the file
        """
        self.file_obj.close()
        file_content = open(self.file_name, 'r')
        result = file_content.read()
        file_content.close()
        self.file_obj = open(self.file_name, self.permissions)
        return result

    def write(self, string):
        """
        Writes a string in the file
        Pre-condition: the file has to exist in the pc
        param str string: the string to write in the file
        returns:
        """
        self.file_obj.write(string)

    @staticmethod
    def value_exists(path, filename, value):
        """
        Looks for a value on the specified file
        Pre-condition: the file has to exist in the pc in the specified path
        param str path: the path of the file
        param str filename: the file name with extension
        param str value: value which is being searched for
        returns: True if found, False if not found
        """
        file_path = os.path.join(path, filename)
        with open(file_path) as file:
            datafile = file.readlines()
            for line in datafile:
                if value in line:
                    return True
        return False

    @staticmethod
    def directory_exists_create(directory_path):
        """
        Checks if a Directory exists, if not the directory is created
        :param directory_path: Path to be checked / created
        :return: none
        """
        if not os.path.exists(directory_path):
            os.makedirs(directory_path)
